﻿using System;

namespace task_00
{
    class MainClass
    {
        class Printer
        {
            readonly ConsoleColor color;

            protected Printer(ConsoleColor color)
            {
                this.color = color;
            }

            public void Print(string value)
            {
                ConsoleColor prevColor = Console.ForegroundColor;

                Console.ForegroundColor = color;

                Console.WriteLine(value);

                Console.ForegroundColor = prevColor;
            }
        }

        class ColorPrinter : Printer
        {
            public ColorPrinter(ConsoleColor color)
                : base(color)
            { }
        }

        public static void Main(string[] args)
        {
            ColorPrinter printer = new ColorPrinter(ConsoleColor.Red);

            printer.Print("Trololo!");

            Printer samePrinter = printer;

            samePrinter.Print("Atata");
            Console.ReadKey();
        }
    }
}
