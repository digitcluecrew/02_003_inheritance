﻿namespace task_03
{
    class Vehicle
    {
        int x;
        int y;
        double price;
        double speed;
        int year;

        public int X { get { return x; } }
        public int Y { get { return y; } }
        public double Price { get { return price; } }
        public double Speed { get { return speed; } }
        public int Year { get { return year; } }

        public Vehicle(int x, int y, double price, double speed, int year)
        {
            this.x = x;
            this.y = y;
            this.price = price;
            this.speed = speed;
            this.year = year;
        }
    }

    class Plane : Vehicle
    {
        public int Height { get; set; }
        public uint PassangersCount { get; set; }

        public Plane(int x, int y, double price, double speed, int year)
            : base(x, y, price, speed, year) { }
    }

    class Car : Vehicle
    {
        public Car(int x, int y, double price, double speed, int year)
            : base(x, y, price, speed, year) { }
    }

    class Ship : Vehicle
    {
        public uint PassangersCount { get; set; }
        public string Port { get; set; }

        public Ship(int x, int y, double price, double speed, int year)
            : base(x, y, price, speed, year) { }
    }
}
