﻿using System;

namespace task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car(100, 200, 100500, 120, 1999);
            Ship myShip = new Ship(100, 200, 100500, 120, 1999);
            Plane myPlane = new Plane(100, 200, 100500, 120, 1999);

            Console.WriteLine("Car:");
            Console.WriteLine("x: {0}", myCar.X);
            Console.WriteLine("y: {0}", myCar.Y);
            Console.WriteLine("Price: {0}", myCar.Price);
            Console.WriteLine("Speed: {0}", myCar.Speed);
            Console.WriteLine("Year: {0}", myCar.Year);

            Console.WriteLine();

            Console.WriteLine("Ship:");
            Console.WriteLine("x: {0}", myShip.X);
            Console.WriteLine("y: {0}", myShip.Y);
            Console.WriteLine("Price: {0}", myShip.Price);
            Console.WriteLine("Speed: {0}", myShip.Speed);
            Console.WriteLine("Year: {0}", myShip.Year);
            Console.WriteLine("Passangers count: {0}", myShip.PassangersCount);
            Console.WriteLine("Port: {0}", myShip.Port);

            Console.WriteLine();

            Console.WriteLine("Plane:");
            Console.WriteLine("x: {0}", myPlane.X);
            Console.WriteLine("y: {0}", myPlane.Y);
            Console.WriteLine("Price: {0}", myPlane.Price);
            Console.WriteLine("Speed: {0}", myPlane.Speed);
            Console.WriteLine("Year: {0}", myPlane.Year);
            Console.WriteLine("Passangers count: {0}", myPlane.PassangersCount);
            Console.WriteLine("Height: {0}", myPlane.Height);

            Console.ReadKey();
        }
    }
}
