﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04
{
    class DocumentWorker
    {

        public DocumentWorker OpenDocument() {
            Console.WriteLine("Document is opened");

            return this;
        }

        public virtual DocumentWorker EditDocument() {
            Console.WriteLine("To edit document buy Pro version");

            return this;
        }

        public virtual DocumentWorker SaveDocument() {
            Console.WriteLine("To save document buy Pro version");

            return this;
        }
    }

    class ProDocumentWorker : DocumentWorker
    {
        public override DocumentWorker EditDocument()
        {
            Console.WriteLine("Document has been edited");

            return this;
        }

        public override DocumentWorker SaveDocument()
        {
            Console.WriteLine("Document has been saved using old extension. To save document using other extensions buy Expert version");

            return this;
        }
    }

    class ExpertDocumentWorker : ProDocumentWorker
    {
        public override DocumentWorker SaveDocument()
        {
            Console.WriteLine("Document has been saved");

            return this;
        }
    }
}
