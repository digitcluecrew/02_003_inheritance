﻿using System;

namespace task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            string key;
            DocumentWorker app;

            Console.WriteLine("Enter product key:");
            key = Console.ReadLine().Trim();

            switch (key)
            {
                case "pro":
                    {
                        Console.WriteLine("Using pro version");
                        app = new ProDocumentWorker();
                        break;
                    }
                case "exp":
                    {
                        Console.WriteLine("Using expert version");
                        app = new ExpertDocumentWorker();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("No product key provided. Using trial");
                        app = new DocumentWorker();
                        break;
                    }

            }

            bool exit = false;

            do
            {               
                Console.Write("Enter operation: ");
                key = Console.ReadLine().Trim();

                switch (key)
                {
                    case "open":
                    case "o":
                        app.OpenDocument();
                        break;
                    case "edit":
                    case "e":
                        app.EditDocument();
                        break;
                    case "save":
                    case "s":
                        app.SaveDocument();
                        break;
                    case "quit":
                    case "exit":
                    case "q":
                        exit = true;
                        break;
                        
                }
                Console.WriteLine();
            } while(!exit);
        }
    }
}
