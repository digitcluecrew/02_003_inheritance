﻿using System;

namespace task_02
{
    public class Pupil
    {
        public virtual void Study() { }
        public virtual void Read() { }
        public virtual void Write() { }
        public virtual void Relax() { }
    }

    class ExcelentPupil : Pupil
    {
        public override void Study() { Console.WriteLine("Study hard!"); }
        public override void Read() { Console.WriteLine("Read good"); }
        public override void Write() {Console.WriteLine("Write excelent"); }
        public override void Relax() { Console.WriteLine("No time to rest!"); }
    }

    class GoodPupil : Pupil
    {
        public override void Study() { Console.WriteLine("Study"); }
        public override void Read() { Console.WriteLine("Read"); }
        public override void Write() { Console.WriteLine("Write"); }
        public override void Relax() { Console.WriteLine("Rest"); }
    }

    class BadPupil : Pupil
    {
        public override void Study() { Console.WriteLine("You know nothing John Snow"); }
        public override void Read() { Console.WriteLine("rea.. what?"); }
        public override void Write() { Console.WriteLine("Write less do more!"); }
        public override void Relax() { Console.WriteLine("Party hard!"); }
    }
}
