﻿using System;

namespace task_02
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            ClassRoom @class = new ClassRoom(
                new ExcelentPupil(),
                new GoodPupil(),
                new BadPupil(),
                new ExcelentPupil()
            );

            @class
                .Study()
                .Read()
                .Relax()
                .Write();

            Console.ReadKey();
        }
    }
}
