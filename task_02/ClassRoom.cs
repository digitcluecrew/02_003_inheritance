﻿using System;
namespace task_02
{
    public class ClassRoom
    {
        Pupil[] students;

        public ClassRoom(Pupil stud1, Pupil stud2)
        {
            students = new Pupil[] { stud1, stud2 };
        }

        public ClassRoom(Pupil stud1, Pupil stud2, Pupil stud3)
        {
            students = new Pupil[] { stud1, stud2, stud3 };
        }

        public ClassRoom(Pupil stud1, Pupil stud2, Pupil stud3, Pupil stud4)
        {
            students = new Pupil[] { stud1, stud2, stud3, stud4 };
        }

        public ClassRoom Study()
        {
            Console.WriteLine("-- Class study! --");
            foreach (Pupil student in students)
            {
                student.Study();
            }

            Console.WriteLine();

            return this;
        }

        public ClassRoom Read()
        {
            Console.WriteLine("-- Class read! --");

            foreach (Pupil student in students)
            {
                student.Read();
            }

            Console.WriteLine();

            return this;
        }

        public ClassRoom Write()
        {
            Console.WriteLine("-- Class write! --");

            foreach (Pupil student in students)
            {
                student.Write();
            }

            Console.WriteLine();

            return this;
        }

        public ClassRoom Relax()
        {
            Console.WriteLine("-- Class relax! --");

            foreach (Pupil student in students)
            {
                student.Relax();
            }

            Console.WriteLine();

            return this;
        }
    }
}
